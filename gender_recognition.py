#!/usr/bin/env python
# coding: utf-8

# # Gender Recoginition Task

# __Prepare Dataset__

# In[2]:


import torch
from torch import nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.models as models   

from torchvision import datasets, models, transforms
import torchvision.transforms.functional as TF      

from PIL import Image                               
import numpy as np
import os


# In[50]:


data_dir = './img/face_gender'

from PIL import Image
from torchvision import transforms
preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

train_datasets = datasets.ImageFolder(os.path.join(data_dir, 'Training'), preprocess)
train_dataloader = torch.utils.data.DataLoader(train_datasets, batch_size=16, shuffle=True, num_workers=4)
print('Training dataset size:', len(train_datasets))

val_datasets = datasets.ImageFolder(os.path.join(data_dir, 'Validation'), preprocess)
val_dataloader = torch.utils.data.DataLoader(val_datasets, batch_size=16, shuffle=True, num_workers=4)
print('Validation dataset size:', len(val_datasets))

class_names = train_datasets.classes
print('Class names:', class_names)


# In[ ]:





# __Prepare model__

# In[118]:


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = models.mobilenet_v2(weights=None)
num_classes = 2
model.classifier = nn.Sequential(
    nn.Dropout(0.2),
    nn.Linear(1280, num_classes)
)
model.to(device)


# __Learning related functions__

# In[116]:


# functions
def train(model, train_dataloader, optimizer, criterion, device):
    model.train()
    loss_list = []
    batch = 0
    for images, labels in iter(train_dataloader):
        images, labels = images.to(device), labels.to(device)
        optimizer.zero_grad()
        model(images)
        outputs = model(images)
        loss = criterion(outputs, labels) 
        loss.backward()
        loss_list.append(loss.detach().cpu().numpy().item())   
        optimizer.step()
        if batch % 700 == 0:
            print(f"{batch=}, {loss=}")
        batch += 1
        # break
    return loss_list

def validate(model, val_dataloader, criterion, device):
    model.eval()
    loss_list = []
    test_error_count = 0
    for images, labels in iter(val_dataloader):
        images, labels = images.to(device), labels.to(device)
        with torch.no_grad():
            outputs = model(images)
            loss = criterion(outputs, labels)
            loss_list.append(loss.detach().cpu().numpy().item())
            test_error_count += float(torch.sum(torch.abs(labels -   
                                outputs.argmax(1))))  
        # break
    test_accuracy = 1.0 - float(test_error_count) / float(len(val_datasets))    
    print(f"accuracy {test_accuracy}")
    return loss_list, test_accuracy


# In[ ]:





# __Learning__

# In[119]:


NUM_EPOCHS = 6
train_losses = []
validation_losses = []
accuracies = []

optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)

for epoch in range(NUM_EPOCHS):
    print(f"{epoch=}")
    print(f"learning rate: {optimizer.param_groups[0]['lr']}")
    train_loss = train(model, train_dataloader, optimizer, F.cross_entropy, device)
    val_loss, accuracy = validate(model, val_dataloader, F.cross_entropy, device)

    # record loss
    train_losses.extend(train_loss)
    validation_losses.extend(val_loss)
    accuracies.append(accuracy)

    scheduler.step()


# In[ ]:





# __Plot Result__

# In[125]:


from matplotlib import pyplot as plt

epochs = range(1, NUM_EPOCHS + 1)

time = range(len(train_losses))

# loss plot
plt.figure(figsize=(10, 5))
plt.plot(range(len(train_losses)), train_losses, label='Training Loss')
plt.plot(range(len(validation_losses)), validation_losses, label='Validation Loss')
plt.title('Training and Validation Losses')
plt.xlabel('Batches')
plt.ylabel('Loss')
plt.ylim(0, 1.5)
plt.legend()
plt.show()

# accuracy plot
plt.figure(figsize=(10, 5))
plt.plot(epochs, accuracies, label='Accuracy')
plt.title('Validation Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.show()


# In[112]:





# In[ ]:




